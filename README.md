Scripts to utilize GitLab GraphQL API
- [Docs](https://docs.gitlab.com/ee/api/graphql/)
- [GraphQL Web API](https://gitlab.com/api/graphql)
- [GraphQL API Ref](https://docs.gitlab.com/ee/api/graphql/reference/)
